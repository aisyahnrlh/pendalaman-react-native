import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native'

const Login = ({login}) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>You haven't logged in yet</Text>
      <TextInput placeholder='Masukkan username' autoCompleteType={'username'} style={styles.inputText}/>
      <TextInput placeholder='Masukkan password' secureTextEntry={true} style={styles.inputText}/>
      <Button title="Login" onPress={login} />
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  inputText: {
    height: 40,
    width: 300,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    margin: 10,
    padding: 10
  }
})