import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { connect } from 'react-redux';

// You can import from local files
import State_Tanpa_Redux from './components/State_Tanpa_Redux';
import CounterApp from './components/CounterApp';
import PizzaTranslator from './components/TextInput';
import UserAuthentication from './components/UserAuthentication';

const inisialState = {
  counter: 0
}

const reducer = (state = inisialState, action) => {
  switch (action.type) {
    case 'INCREASE_COUNTER':
      return { counter: state.counter + 1 }
    case 'DECREASE_COUNTER':
      return { counter: state.counter - 1 }
  }
  return state
}

const store = createStore(reducer)

export default function App() {
  return (
    //<State_Tanpa_Redux/>
    //<Provider store={store}>
    //  <CounterApp />
    //</Provider>
    //<PizzaTranslator/>
    <UserAuthentication/>
  );
}
